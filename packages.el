;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; APPS
(package! define-word)
(package! zoom)
(package! nov)
(package! guix)

;; UTILITIES
(package! plz)
(package! exec-path-from-shell)
(package! vertico-posframe)
;; https://github.com/tumashu/vertico-posframe
;;(package! outshine)
(package! olivetti)
;;(package! gptel)

;; modes
(package! meson-mode)
;;(package! smalltalk-mode)
;;(package! hare-mode :recipe (:repo "https://git.sr.ht/~bbuccianti/hare-mode"))
;;(package! janet-mode)
;; (package! emacs-prisma-mode :recipe (:repo "https://github.com/pimeys/emacs-prisma-mode"))
;;(package! adoc-mode)
;; (package! gdscript-mode
;;   :recipe (:host github :repo "godotengine/emacs-gdscript-mode"))

;; bugfix
;;(package! transient :pin "c2bdf7e12c530eb85476d3aef317eb2941ab9440")
;;(package! with-editor :pin "391e76a256aeec6b9e4cbd733088f30c677d965b")
