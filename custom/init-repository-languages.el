;;; -*- lexical-binding: t;

;; https://download.eclipse.org/jdtls/milestones/?d
(setq! lsp-java-jdt-download-url
       "https://www.eclipse.org/downloads/download.php?file=/jdtls/milestones/1.32.0/jdt-language-server-1.32.0-202402011424.tar.gz")


;; (add-to-list 'load-path "/home/easbarba/.opam/default/share/emacs/site-lisp")
;; (require 'ocp-indent)

;; ================================
;; ADDITIONAL LANGUAGES TOOLINGS
;; ================================

(after! apheleia
  (setq apheleia-disabled-modes '(nxml-mode))
  ;; If you want to disable for more modes, add them to the list:
  ;; (setq apheleia-disabled-modes '(nxml-mode css-mode))
  )

;; (setq-hook! 'nxml-mode-hook +format-with-lsp nil)
;; (setq! +format-on-save-enabled-modes
;;        '(not emacs-lisp-mode nxml-mode php-mode c-mode))

;; (after! lsp-mode
;;   (set-lsp-priority! 'iph 111))

(set-ligatures! '(java-mode)
  ;; Functional
  ;; :def "void "
  ;; Types
  :null "null"
  :true "true" :false "false"
  :int "int" :float "float"
  ;; :str "std::string"
  :bool "bool"
  ;; Flow
  :not "!"
  :and "&&" :or "||"
  :for "for"
  :return "return"
  :yield "import")


;; (require 'f)
;; (setq lsp-dart-sdk-dir (f-join *home* ".local/flutter/bin/cache/dart-sdk/"))

(provide 'init-repository-languages)
