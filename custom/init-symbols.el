;;; init-symbols.el --- Keyboard Symbols -*- lexical-binding: t; -*-
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;; Keyboard Symbols
;;; Code:

;; (defhydra hydra-symbols (global-map "C-c z")
;;   "symbols"
;;   ("z" (lambda () (interactive) (insert "()")) "parens")
;;   ("x" (lambda () (interactive) (insert "[]")) "square brackets")
;;   ("d" (lambda () (interactive) (insert "{}")) "curly brackets")
;;   ("c" (lambda () (interactive) (insert "=")) "equals")
;;   ("a" (lambda () (interactive) (insert "->")) "thin arrow")
;;   ("s" (lambda () (interactive) (insert "=>")) "thick arrow")
;;   ("w" (lambda () (interactive) (insert "<?php")) "php/header")
;;   ("e" (lambda () (interactive) (insert "#+begin_src php\n<?php\n#+end_src"))
;;    "php/org-mode source block")
;;   ("q" (lambda () (interactive) (insert "declare(strict_types=1);"))
;;    "php/strict type"))

(provide 'init-symbols)
;;; init-symbols.el ends here
