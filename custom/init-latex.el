;;; init-latex.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Attrako Null
;;
;; Author: Attrako Null <attrako@tutanota.com>
;; Maintainer: Attrako Null <attrako@tutanota.com>
;; Created: February 21, 2024
;; Modified: February 21, 2024
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/easbarba/init-latex
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(setq! org-latex-hyperref-template "\\hypersetup{\n pdfauthor={%a},\n pdftitle={%t},\n pdfkeywords={%k},
 pdfsubject={%d},\n pdfcreator={%c},\n pdflang={%L},\n colorlinks=true}\n,\n linkcolor=electricblue")


(provide 'init-latex)
;;; init-latex.el ends here
