;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(when (string-equal (system-name) "attrako-x220")
  (setq! straight-disable-native-compile t))

(remove-hook 'after-init-hook #'debian-ispell-set-startup-menu)

(require 'iso-transl) ;; solves dbus keyboard conflict
(setq! frame-resize-pixelwise t) ;; do not resize initial window size

(when (member "JetBrains Mono" (font-family-list))
  (setq! doom-font (font-spec :family "JetBrains Mono" :size 22)
         doom-variable-pitch-font (font-spec :family "sans" :size 24))

  (when (string-equal (system-name) "attrako-x220")
    (setq! doom-font (font-spec :family "JetBrains Mono" :size 18)
           doom-variable-pitch-font (font-spec :family "sans" :size 20))))


(setq! user-full-name "Attrako Null"
       user-mail-address "attrako@tutanota.com"
       user-real-login-name "attrako"
       doom-theme 'doom-spacegrey ;; 1337 for terminal
       display-line-numbers-type nil
       org-directory "~/Documents/org"
       doom-modeline-buffer-file-name-style 'truncate-with-project)

;; BUILT-IN
(load! "custom/init-utils")
(load! "custom/init-variables")
(load! "custom/init-dired")
(load! "custom/init-functions")
(load! "custom/init-symbols")
(load! "custom/init-misc")
(load! "custom/init-builtin")
(load! "custom/init-snippets")
(load! "custom/init-repository-misc")
(load! "custom/init-repository-languages")
